var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { LoadingController } from 'ionic-angular';
//import '../../ng2-ripple-directive/src/scss/ripple.scss';
var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, httpProvider, loadingCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.httpProvider = httpProvider;
        this.loadingCtrl = loadingCtrl;
        this.items = [];
        this.itemExpandHeight = 100;
        this.toggled = false;
        this.itemExpandHeight = 300;
        this.list = [];
        this.invoice = null;
        this.url = {
            randomUser: 'https://randomuser.me/api/?results=5'
        };
        this.toggled = false;
        var loader = this.loadingCtrl.create({
            content: "Please wait...",
        });
        loader.present();
        this.loadPeople(this.url.randomUser)
            .then(function (data) {
            //this.list = data;
            for (var i = 0; i < data.length; i++) {
                _this.item = data[i];
                _this.item.expanded = false;
                _this.items.push(_this.item);
            }
            console.log(data[0]);
            _this.invoice = data[0];
            loader.dismiss();
        })
            .catch(function (data) {
            loader.dismiss();
        });
    }
    HomePage.prototype.loadPeople = function (url) {
        var _this = this;
        this.list = [];
        return this.httpProvider.load(url)
            .then(function (data) {
            _this.httpResponse = data;
            if (_this.httpResponse) {
                //this.list = this.httpResponse.results;
                return _this.httpResponse.results;
            }
        });
    };
    HomePage.prototype.expandItem = function (item) {
        var _this = this;
        this.items.map(function (listItem) {
            if (item == listItem) {
                listItem.expanded = !listItem.expanded;
            }
            else {
                listItem.expanded = false;
            }
            _this.invoice = item;
            return listItem;
        });
    };
    HomePage.prototype.toggle = function () {
        this.toggled = this.toggled ? false : true;
    };
    HomePage = __decorate([
        Component({
            selector: 'page-home',
            templateUrl: 'home.html'
        }),
        __metadata("design:paramtypes", [NavController, HttpProvider, LoadingController])
    ], HomePage);
    return HomePage;
}());
export { HomePage };
var result = [{ "ApplicationNumber": "NPBD/EZ/000000029", "Company": "FRL", "LOBName": "E Zone", "BankName": "BOI", "ApplicationDate": "43123", "DueDate": "43213", "UserRemarks": "", "BatchNumber": "BPBD/EZ/0221/17-18", "ApplicationAmount": "541681.54", "SupplierName": "EZ-Usha Interna" }, { "ApplicationNumber": "NPBD/HF/000000017", "Company": "FRL", "LOBName": "Home Fashion", "BankName": "CORP", "ApplicationDate": "43117", "DueDate": "43207", "UserRemarks": "", "BatchNumber": "BPBD/HF/0185/17-18", "ApplicationAmount": "1648841.88", "SupplierName": "Noya Life Style" }, { "ApplicationNumber": "NPBD/HF/000000017", "Company": "FRL", "LOBName": "Home Fashion", "BankName": "CORP", "ApplicationDate": "43117", "DueDate": "43207", "UserRemarks": "", "BatchNumber": "BPBD/HF/0185/17-18", "ApplicationAmount": "1648841.88", "SupplierName": "Noya Life Style" }, { "ApplicationNumber": "NPBD/HF/000000017", "Company": "FRL", "LOBName": "Home Fashion", "BankName": "CORP", "ApplicationDate": "43117", "DueDate": "43207", "UserRemarks": "", "BatchNumber": "BPBD/HF/0185/17-18", "ApplicationAmount": "1648841.88", "SupplierName": "Noya Life Style" }, { "ApplicationNumber": "NPBD/HF/000000017", "Company": "FRL", "LOBName": "Home Fashion", "BankName": "CORP", "ApplicationDate": "43117", "DueDate": "43207", "UserRemarks": "", "BatchNumber": "BPBD/HF/0185/17-18", "ApplicationAmount": "1648841.88", "SupplierName": "Noya Life Style" }, { "ApplicationNumber": "NPBD/HF/000000017", "Company": "FRL", "LOBName": "Home Fashion", "BankName": "CORP", "ApplicationDate": "43117", "DueDate": "43207", "UserRemarks": "", "BatchNumber": "BPBD/HF/0185/17-18", "ApplicationAmount": "1648841.88", "SupplierName": "Noya Life Style" }, { "ApplicationNumber": "NPBD/HF/000000017", "Company": "FRL", "LOBName": "Home Fashion", "BankName": "CORP", "ApplicationDate": "43117", "DueDate": "43207", "UserRemarks": "", "BatchNumber": "BPBD/HF/0185/17-18", "ApplicationAmount": "1648841.88", "SupplierName": "Noya Life Style" }, { "ApplicationNumber": "NPBD/HF/000000017", "Company": "FRL", "LOBName": "Home Fashion", "BankName": "CORP", "ApplicationDate": "43117", "DueDate": "43207", "UserRemarks": "", "BatchNumber": "BPBD/HF/0185/17-18", "ApplicationAmount": "1648841.88", "SupplierName": "Noya Life Style" }, { "ApplicationNumber": "NPBD/HF/000000017", "Company": "FRL", "LOBName": "Home Fashion", "BankName": "CORP", "ApplicationDate": "43117", "DueDate": "43207", "UserRemarks": "", "BatchNumber": "BPBD/HF/0185/17-18", "ApplicationAmount": "1648841.88", "SupplierName": "Noya Life Style" }, { "ApplicationNumber": "NPBD/HF/000000017", "Company": "FRL", "LOBName": "Home Fashion", "BankName": "CORP", "ApplicationDate": "43117", "DueDate": "43207", "UserRemarks": "", "BatchNumber": "BPBD/HF/0185/17-18", "ApplicationAmount": "1648841.88", "SupplierName": "Noya Life Style" }, { "ApplicationNumber": "NPBD/HF/000000017", "Company": "FRL", "LOBName": "Home Fashion", "BankName": "CORP", "ApplicationDate": "43117", "DueDate": "43207", "UserRemarks": "", "BatchNumber": "BPBD/HF/0185/17-18", "ApplicationAmount": "1648841.88", "SupplierName": "Noya Life Style" }];
//# sourceMappingURL=home.js.map