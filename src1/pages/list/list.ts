import { Component,ViewChild,AfterViewInit,NgZone   } from '@angular/core';
import { NavController,NavParams ,LoadingController,ModalController} from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { Content } from 'ionic-angular';
import * as $ from 'jquery'
import {InvoiceDetailPage} from '../../pages/invoice-detail/invoice-detail';
import { PopoverController } from 'ionic-angular';
import { PopoverPage } from '../../pages/list/Popover/Popover';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage implements AfterViewInit{
	@ViewChild(Content) content: Content;
	list : string[];
	maleList : string[];
	femaleList : string[];
	count : {male : number, female: number};
	selectedItem : any;
	data : any;
	items : Array<{
		gender:string,
		name:{title:string,first:string,last:string},
		location : {street:string,city:string,state:string,postCode:string},
		email: string,
		picture:{large : string, medium:string, thumbnail:string}
	}>;
	resItem : any;
	httpResponse : any;
	url : {randomUser : string, jsonUrl : string};
	gender: string = "Male";
	itemSelected : boolean = false;
	itemSelectedIndex : number;
	peopleInfo :any;
	scrollHeight: any;
	elScroll : any;
	loginStatus : any;
  constructor(public navCtrl: NavController, public httpService : HttpProvider,public navParams: NavParams,public loadingCtrl: LoadingController,public modalCtrl: ModalController,public popoverCtrl: PopoverController) {
	this.list = [];
  	this.maleList = [];
	this.femaleList = [];
	this.count = {male : 0, female: 0};
  	this.selectedItem = navParams.get('item');
  	this.peopleInfo = null;
  	this.url = {
  		randomUser : 'https://randomuser.me/api/?results=50',
  		jsonUrl : '/assets/json/listofinvoice.json'
  	};
  	this.itemSelected = false;
  	this.loginStatus = navParams.get("loginStatus");
  	if (this.loginStatus != undefined && this.loginStatus == true) {
  		this.onLoadData();
  	}else{
  		this.getLocalData();
  	}
	
  }
  onLoadData(){
  	let loader = this.loadingCtrl.create({
      content: "Please wait...",
      //duration: 3000
    });
    loader.present();
	this.loadPeople(this.url.jsonUrl)
	.then(data => {
		
		if (data.ListOfInvoices) {

			this.list =data.ListOfInvoices.ListOfInvoice;
			localStorage.setItem("ListOfInvocies", JSON.stringify(data));
			if (this.list.length > 0) {
				console.log(this.list[0]);
			}
				
		}
		//this.count = {male : this.maleList.length, female: this.femaleList.length};
		loader.dismiss();
	})
	.catch(function(data){
		loader.dismiss();	
	});
  }
  ngAfterViewInit() {
  	this.content.ionScroll.subscribe(($event) => {
  		if (!$event.contentElement.className.includes('expanded')) {
  			this.scrollHeight = $event.scrollTop;
  		}
    });
  }
  getLocalData(){
  	var objJson = localStorage.getItem("ListOfInvocies");
  	let listJson :any = [];
  	if (objJson != null) {
  		 listJson = JSON.parse(objJson);
  		 if (listJson) {
  		 	this.list = listJson.ListOfInvoices.ListOfInvoice;
  		 }

  	}
  }
  loadPeople(url){
  	this.list = [];
	  return this.httpService.load(url)
	  .then(data => {
	    this.httpResponse = data;
	    if (this.httpResponse) {
	    	return this.httpResponse;
	    }
	  });
	}
	itemTapped(event, item, index) {
			var body = document.getElementsByTagName('body');
			var listbtn = document.getElementById('listbtn');
			if (this.itemSelected == true) {
				this.itemSelected = false;
				//document.body.className = '';
				this.itemSelectedIndex = index;
			}else{
				this.itemSelected = true;
				//document.body.className = 'expanded';
			}
			var listButton = $("#listbtn>button");
			listButton.addClass('inactive');
			var list = $(listButton[index]);
			list.removeClass('inactive');
			list.addClass('active');

			var tabs = $('.tabbar');
			tabs.fadeOut(500);
			//tabs.addClass('inActive');
			setTimeout(function(){
				tabs.hide();	
			},500)
			
			this.peopleInfo = item;
			this.scrollToTop();
	  }
	  resetList(){
	  		var listbtn = document.getElementById('listbtn');
			var listButton = $('listbtn'); 
			$("#listbtn>button").removeClass("active");
			$("#listbtn>button").removeClass("inactive");
			this.itemSelected = false;
			if (this.scrollHeight != null) {
				this.content.scrollTo(10,this.scrollHeight,0);

			}
			var tabs = $('.tabbar');
			tabs.show();
	  }
	  scrollToTop() {
	    this.content.scrollToTop(100);
	  }
	  goToInvoiceDetail(event, item, index) {
    // That's right, we're pushing to ourselves!
	    this.navCtrl.push(InvoiceDetailPage, {
	      item: item,
	      index : index
	    });
	  }

	ionViewDidLoad() {
    //console.log("I'm alive!");
	  }
	  ionViewWillLeave() {
	  /// console.log("Looks like I'm about to leave :(");
	  }
	  ionViewDidEnter(){
  		//this.onLoadData();
  		//this.sync();
  		//console.log("I'm Enter!");
  		}

  	presentPopover(myEvent) {
	    let popover = this.popoverCtrl.create(PopoverPage, {cb:
	    	function(listPage){
	    		//listPage.sync();
	    		this.viewCtrl.dismiss('popover').then(() => {
	    			//this.sync();
	    		})
	    	}
	     });
	    popover.present({
	      ev: myEvent
	    });
  	}
  	public sync(){
  		this.onLoadData();
  	}
  }


