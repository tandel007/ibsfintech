import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { LoadingController } from 'ionic-angular';
//import { RippleDirective } from 'ng2-ripple-directive';

//import '../../ng2-ripple-directive/src/scss/ripple.scss';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
	items: any = [];
	item : any;
	itemExpandHeight: number = 100;
	url : any;
	httpResponse :any;
	list : any;
	invoice : any;
	public toggled: boolean = false;
constructor(public navCtrl: NavController,public httpProvider : HttpProvider,public loadingCtrl: LoadingController ) {
  	this.itemExpandHeight = 300;
  	this.list = [];
  	this.invoice = null;
  	this.url = {
  		randomUser : 'https://randomuser.me/api/?results=5'
  	};
	this.toggled = false;
	let loader = this.loadingCtrl.create({
      content: "Please wait...",
      //duration: 3000
    });
    loader.present();
	this.loadPeople(this.url.randomUser)
	.then(data => {
		//this.list = data;
		for(let i=0; i < data.length; i++){
			this.item = data[i];
			this.item.expanded = false;
			this.items.push(this.item);
		}
		console.log(data[0]);
		this.invoice = data[0];
		loader.dismiss();
	})
	.catch(function(data){
		loader.dismiss();	
	});
  }
  loadPeople(url){
  	this.list = [];
	  return this.httpProvider.load(url)
	  .then(data => {
	    this.httpResponse = data;
	    if (this.httpResponse) {
	    	//this.list = this.httpResponse.results;
	    	return this.httpResponse.results;
	    }
	  });
	}
expandItem(item){
 
        this.items.map((listItem) => {
 
            if(item == listItem){
                listItem.expanded = !listItem.expanded;
            } else {
                listItem.expanded = false;
            }
			this.invoice = item;
            return listItem;
 
        });
 
    }
    public toggle(): void {
       this.toggled = this.toggled ? false : true;
    }
}
var result = [{"ApplicationNumber":"NPBD/EZ/000000029","Company":"FRL","LOBName":"E Zone","BankName":"BOI","ApplicationDate":"43123","DueDate":"43213","UserRemarks":"","BatchNumber":"BPBD/EZ/0221/17-18","ApplicationAmount":"541681.54","SupplierName":"EZ-Usha Interna"},{"ApplicationNumber":"NPBD/HF/000000017","Company":"FRL","LOBName":"Home Fashion","BankName":"CORP","ApplicationDate":"43117","DueDate":"43207","UserRemarks":"","BatchNumber":"BPBD/HF/0185/17-18","ApplicationAmount":"1648841.88","SupplierName":"Noya Life Style"},{"ApplicationNumber":"NPBD/HF/000000017","Company":"FRL","LOBName":"Home Fashion","BankName":"CORP","ApplicationDate":"43117","DueDate":"43207","UserRemarks":"","BatchNumber":"BPBD/HF/0185/17-18","ApplicationAmount":"1648841.88","SupplierName":"Noya Life Style"},{"ApplicationNumber":"NPBD/HF/000000017","Company":"FRL","LOBName":"Home Fashion","BankName":"CORP","ApplicationDate":"43117","DueDate":"43207","UserRemarks":"","BatchNumber":"BPBD/HF/0185/17-18","ApplicationAmount":"1648841.88","SupplierName":"Noya Life Style"},{"ApplicationNumber":"NPBD/HF/000000017","Company":"FRL","LOBName":"Home Fashion","BankName":"CORP","ApplicationDate":"43117","DueDate":"43207","UserRemarks":"","BatchNumber":"BPBD/HF/0185/17-18","ApplicationAmount":"1648841.88","SupplierName":"Noya Life Style"},{"ApplicationNumber":"NPBD/HF/000000017","Company":"FRL","LOBName":"Home Fashion","BankName":"CORP","ApplicationDate":"43117","DueDate":"43207","UserRemarks":"","BatchNumber":"BPBD/HF/0185/17-18","ApplicationAmount":"1648841.88","SupplierName":"Noya Life Style"},{"ApplicationNumber":"NPBD/HF/000000017","Company":"FRL","LOBName":"Home Fashion","BankName":"CORP","ApplicationDate":"43117","DueDate":"43207","UserRemarks":"","BatchNumber":"BPBD/HF/0185/17-18","ApplicationAmount":"1648841.88","SupplierName":"Noya Life Style"},{"ApplicationNumber":"NPBD/HF/000000017","Company":"FRL","LOBName":"Home Fashion","BankName":"CORP","ApplicationDate":"43117","DueDate":"43207","UserRemarks":"","BatchNumber":"BPBD/HF/0185/17-18","ApplicationAmount":"1648841.88","SupplierName":"Noya Life Style"},{"ApplicationNumber":"NPBD/HF/000000017","Company":"FRL","LOBName":"Home Fashion","BankName":"CORP","ApplicationDate":"43117","DueDate":"43207","UserRemarks":"","BatchNumber":"BPBD/HF/0185/17-18","ApplicationAmount":"1648841.88","SupplierName":"Noya Life Style"},{"ApplicationNumber":"NPBD/HF/000000017","Company":"FRL","LOBName":"Home Fashion","BankName":"CORP","ApplicationDate":"43117","DueDate":"43207","UserRemarks":"","BatchNumber":"BPBD/HF/0185/17-18","ApplicationAmount":"1648841.88","SupplierName":"Noya Life Style"},{"ApplicationNumber":"NPBD/HF/000000017","Company":"FRL","LOBName":"Home Fashion","BankName":"CORP","ApplicationDate":"43117","DueDate":"43207","UserRemarks":"","BatchNumber":"BPBD/HF/0185/17-18","ApplicationAmount":"1648841.88","SupplierName":"Noya Life Style"}];

