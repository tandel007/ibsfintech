var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, LoadingController, ModalController } from 'ionic-angular';
import { HttpProvider } from '../../providers/http/http';
import { Content } from 'ionic-angular';
import * as $ from 'jquery';
import { InvoiceDetailPage } from '../../pages/invoice-detail/invoice-detail';
import { PopoverController } from 'ionic-angular';
import { PopoverPage } from '../../pages/list/Popover/Popover';
var ListPage = /** @class */ (function () {
    function ListPage(navCtrl, httpService, navParams, loadingCtrl, modalCtrl, popoverCtrl) {
        this.navCtrl = navCtrl;
        this.httpService = httpService;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.modalCtrl = modalCtrl;
        this.popoverCtrl = popoverCtrl;
        this.gender = "Male";
        this.itemSelected = false;
        this.list = [];
        this.maleList = [];
        this.femaleList = [];
        this.count = { male: 0, female: 0 };
        this.selectedItem = navParams.get('item');
        this.peopleInfo = null;
        this.url = {
            randomUser: 'https://randomuser.me/api/?results=50',
            jsonUrl: 'assets/json/listofinvoice.json'
        };
        this.itemSelected = false;
        this.loginStatus = navParams.get("loginStatus");
        if (this.loginStatus != undefined && this.loginStatus == true) {
            this.onLoadData();
        }
        else {
            this.getLocalData();
        }
    }
    ListPage.prototype.onLoadData = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Please wait...",
        });
        loader.present();
        this.loadPeople(this.url.jsonUrl)
            .then(function (data) {
            if (data.ListOfInvoices) {
                _this.list = data.ListOfInvoices.ListOfInvoice;
                localStorage.setItem("ListOfInvocies", JSON.stringify(data));
                if (_this.list.length > 0) {
                    console.log(_this.list[0]);
                }
            }
            //this.count = {male : this.maleList.length, female: this.femaleList.length};
            loader.dismiss();
        })
            .catch(function (data) {
            loader.dismiss();
        });
    };
    ListPage.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.content.ionScroll.subscribe(function ($event) {
            if (!$event.contentElement.className.includes('expanded')) {
                _this.scrollHeight = $event.scrollTop;
            }
        });
    };
    ListPage.prototype.getLocalData = function () {
        var objJson = localStorage.getItem("ListOfInvocies");
        var listJson = [];
        if (objJson != null) {
            listJson = JSON.parse(objJson);
            if (listJson) {
                this.list = listJson.ListOfInvoices.ListOfInvoice;
            }
        }
    };
    ListPage.prototype.loadPeople = function (url) {
        var _this = this;
        this.list = [];
        return this.httpService.load(url)
            .then(function (data) {
            _this.httpResponse = data;
            if (_this.httpResponse) {
                return _this.httpResponse;
            }
        });
    };
    ListPage.prototype.itemTapped = function (event, item, index) {
        var body = document.getElementsByTagName('body');
        var listbtn = document.getElementById('listbtn');
        if (this.itemSelected == true) {
            this.itemSelected = false;
            //document.body.className = '';
            this.itemSelectedIndex = index;
        }
        else {
            this.itemSelected = true;
            //document.body.className = 'expanded';
        }
        var listButton = $("#listbtn>button");
        listButton.addClass('inactive');
        var list = $(listButton[index]);
        list.removeClass('inactive');
        list.addClass('active');
        var tabs = $('.tabbar');
        tabs.fadeOut(500);
        //tabs.addClass('inActive');
        setTimeout(function () {
            tabs.hide();
        }, 500);
        this.peopleInfo = item;
        this.scrollToTop();
    };
    ListPage.prototype.resetList = function () {
        var listbtn = document.getElementById('listbtn');
        var listButton = $('listbtn');
        $("#listbtn>button").removeClass("active");
        $("#listbtn>button").removeClass("inactive");
        this.itemSelected = false;
        if (this.scrollHeight != null) {
            this.content.scrollTo(10, this.scrollHeight, 0);
        }
        var tabs = $('.tabbar');
        tabs.show();
    };
    ListPage.prototype.scrollToTop = function () {
        this.content.scrollToTop(100);
    };
    ListPage.prototype.goToInvoiceDetail = function (event, item, index) {
        // That's right, we're pushing to ourselves!
        this.navCtrl.push(InvoiceDetailPage, {
            item: item,
            index: index
        });
    };
    ListPage.prototype.ionViewDidLoad = function () {
        //console.log("I'm alive!");
    };
    ListPage.prototype.ionViewWillLeave = function () {
        /// console.log("Looks like I'm about to leave :(");
    };
    ListPage.prototype.ionViewDidEnter = function () {
        //this.onLoadData();
        //this.sync();
        //console.log("I'm Enter!");
    };
    ListPage.prototype.presentPopover = function (myEvent) {
        var popover = this.popoverCtrl.create(PopoverPage, { cb: function (listPage) {
                //listPage.sync();
                this.viewCtrl.dismiss('popover').then(function () {
                    //this.sync();
                });
            }
        });
        popover.present({
            ev: myEvent
        });
    };
    ListPage.prototype.sync = function () {
        this.onLoadData();
    };
    __decorate([
        ViewChild(Content),
        __metadata("design:type", Content)
    ], ListPage.prototype, "content", void 0);
    ListPage = __decorate([
        Component({
            selector: 'page-list',
            templateUrl: 'list.html'
        }),
        __metadata("design:paramtypes", [NavController, HttpProvider, NavParams, LoadingController, ModalController, PopoverController])
    ], ListPage);
    return ListPage;
}());
export { ListPage };
//# sourceMappingURL=list.js.map