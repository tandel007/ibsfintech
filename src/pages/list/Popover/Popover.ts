import { Component,ViewChild,AfterViewInit   } from '@angular/core';
import { NavController,NavParams ,ViewController} from 'ionic-angular';
import { ListPage } from '../list';
import { LoginPage } from '../../../pages/login/login';
import { AlertController } from 'ionic-angular';

@Component({
  selector: 'page-popover',
  templateUrl:  'Popover.html'
})

export class PopoverPage {
  page : any;
  listpage : any;
  callback : any;
  confirm : any;
  listEvent : any;
  constructor(public viewCtrl: ViewController,public navCtrl: NavController, public navParams: NavParams,public alertCtrl: AlertController) {
    this.listpage = ListPage;
    this.callback = navParams.get("cb");
    this.listEvent = navParams.get("event");
  }

  close() {
    //this.listpage.sync();
    this.callback(this.listEvent);
    //this.viewCtrl.dismiss();
    //this.navCtrl.setRoot(ListPage,{loginStatus: true});
    //this.listEvent.sync();
  }
  logout(){
    localStorage.clear();
    this.navCtrl.setRoot(LoginPage);
  }
  closePopoverView(){
      this.viewCtrl.dismiss('popover').then(()=>{
           this.viewCtrl.dismiss('alert').then(()=>{
             this.logout();
           });
      });
  }

  showConfirm() {
    
    this.confirm = this.alertCtrl.create({
      title: 'Are you sure to exit?',
      message: '',//'Do you agree to use this lightsaber to do good across the intergalactic galaxy?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            this.dismissAlert();
          }
        },
        {
          text: 'Ok',
          handler: () => {
            try{
              //this.dismissAlert();
              this.closePopoverView();
              
                           
                console.log('Agree clicked');
            }catch(e){
                //this.navCtrl.setRoot(LoginPage);
            }
            
          }
        }
      ]
    });
    this.confirm.present();
  }
  dismissAlert(){
    this.viewCtrl.dismiss('popover').then(()=>{
           this.viewCtrl.dismiss('alert').then(()=>{
             //this.logout();
           });
      });
  }
}