var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { ListPage } from '../list';
import { LoginPage } from '../../../pages/login/login';
import { AlertController } from 'ionic-angular';
var PopoverPage = /** @class */ (function () {
    function PopoverPage(viewCtrl, navCtrl, navParams, alertCtrl) {
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.listpage = ListPage;
        this.callback = navParams.get("cb");
    }
    PopoverPage.prototype.close = function () {
        //this.listpage.sync();
        //this.callback();
        //this.viewCtrl.dismiss();
        this.navCtrl.setRoot(ListPage, { loginStatus: true });
    };
    PopoverPage.prototype.logout = function () {
        localStorage.clear();
        this.navCtrl.setRoot(LoginPage);
    };
    PopoverPage.prototype.closePopoverView = function () {
        var _this = this;
        this.viewCtrl.dismiss('popover').then(function () {
            _this.viewCtrl.dismiss('alert').then(function () {
                _this.logout();
            });
        });
    };
    PopoverPage.prototype.showConfirm = function () {
        var _this = this;
        this.confirm = this.alertCtrl.create({
            title: 'Are you sure to exit?',
            message: '',
            buttons: [
                {
                    text: 'Cancel',
                    handler: function () {
                        _this.dismissAlert();
                    }
                },
                {
                    text: 'Ok',
                    handler: function () {
                        try {
                            //this.dismissAlert();
                            _this.closePopoverView();
                            console.log('Agree clicked');
                        }
                        catch (e) {
                            //this.navCtrl.setRoot(LoginPage);
                        }
                    }
                }
            ]
        });
        this.confirm.present();
    };
    PopoverPage.prototype.dismissAlert = function () {
        var _this = this;
        this.viewCtrl.dismiss('popover').then(function () {
            _this.viewCtrl.dismiss('alert').then(function () {
                //this.logout();
            });
        });
    };
    PopoverPage = __decorate([
        Component({
            selector: 'page-popover',
            templateUrl: 'Popover.html'
        }),
        __metadata("design:paramtypes", [ViewController, NavController, NavParams, AlertController])
    ], PopoverPage);
    return PopoverPage;
}());
export { PopoverPage };
//# sourceMappingURL=Popover.js.map