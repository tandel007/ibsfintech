import { Component } from '@angular/core';
import { NavController ,LoadingController,ModalController,ToastController } from 'ionic-angular';

import { HomePage } from '../home/home';
import { ListPage } from '../list/list';
import { HttpProvider } from '../../providers/http/http';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
	registerCredentials = { email: '', password: '' };
  url : {login:string}
  constructor(public navCtrl: NavController, public httpService : HttpProvider,public loadingCtrl: LoadingController,public modalCtrl: ModalController,private toastCtrl: ToastController) {
  this.url = {
       login : 'assets/json/listofinvoice.json'
    };
  }


  loadData(){
  	alert("Done");
  }
  createAccount(){
  	alert("Coming Soon");	
  }
  loginAccount(){
    this.onLoadData().then(flag =>{
        if (flag == true) {
          localStorage.setItem("userDetail", JSON.stringify(this.registerCredentials));
          this.navCtrl.setRoot(ListPage,{userDetail : this.registerCredentials, loginStatus : true});
        }else{
          this.presentToast('Please enter valid credentials');
        }
    });

    
  }

   onLoadData(){
    let loader = this.loadingCtrl.create({
        content: "Please wait...",
        //duration: 3000
      });
      loader.present();
        return this.loadUserlist(this.url.login)
        .then(data => {
          let result : any = data;
          if (result.Userlist) {
              for(let i=0; i < result.Userlist.length; i++){
                let obj : any = result.Userlist[i];
                  if (this.registerCredentials.email == obj.Username && this.registerCredentials.password == obj.Password) {
                    loader.dismiss(); 
                    return true;
                  }
              }
           }
          //this.count = {male : this.maleList.length, female: this.femaleList.length};
          loader.dismiss();
          return false;  
        })
        .catch(function(data){
          loader.dismiss(); 
          return false;
        });
    }

    loadUserlist(url){
      return this.httpService.load(url)
      .then(data => {
        return data;
      });
    }

    presentToast(msg) {
      let toast = this.toastCtrl.create({
        message: msg,
        duration: 2000,
        position: 'bottom',
        cssClass: 'danger'
      });

      toast.onDidDismiss(() => {
        console.log('Dismissed toast');
      });

      toast.present();
    }
}
