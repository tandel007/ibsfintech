var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { NavController, LoadingController, ModalController } from 'ionic-angular';
import { ListPage } from '../list/list';
import { HttpProvider } from '../../providers/http/http';
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, httpService, loadingCtrl, modalCtrl) {
        this.navCtrl = navCtrl;
        this.httpService = httpService;
        this.loadingCtrl = loadingCtrl;
        this.modalCtrl = modalCtrl;
        this.registerCredentials = { email: '', password: '' };
        this.url = {
            login: 'assests/json/Userlist.json'
        };
    }
    LoginPage.prototype.loadData = function () {
        alert("Done");
    };
    LoginPage.prototype.createAccount = function () {
        alert("Coming Soon");
    };
    LoginPage.prototype.loginAccount = function () {
        localStorage.setItem("userDetail", JSON.stringify(this.registerCredentials));
        this.navCtrl.setRoot(ListPage, { userDetail: this.registerCredentials, loginStatus: true });
    };
    LoginPage.prototype.onLoadData = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Please wait...",
        });
        loader.present();
        this.loadUserlist(this.url.login)
            .then(function (data) {
            if (data.login) {
                _this.list = data.ListOfInvoices.ListOfInvoice;
                localStorage.setItem("ListOfInvocies", JSON.stringify(data));
                if (_this.list.length > 0) {
                    console.log(_this.list[0]);
                }
            }
            //this.count = {male : this.maleList.length, female: this.femaleList.length};
            loader.dismiss();
        })
            .catch(function (data) {
            loader.dismiss();
        });
    };
    LoginPage.prototype.loadUserlist = function (url) {
        return this.httpService.load(url)
            .then(function (data) {
            return data;
        });
    };
    LoginPage = __decorate([
        Component({
            selector: 'page-login',
            templateUrl: 'login.html'
        }),
        __metadata("design:paramtypes", [NavController, HttpProvider, LoadingController, ModalController])
    ], LoginPage);
    return LoginPage;
}());
export { LoginPage };
//# sourceMappingURL=login.js.map