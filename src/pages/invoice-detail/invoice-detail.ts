import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import { ListPage } from '../../pages/list/list';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the InvoiceDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-invoice-detail',
  templateUrl: 'invoice-detail.html',
})
export class InvoiceDetailPage {
	peopleInfo :any;
	ListOfInvoices : any;
	index : number;
	confirm : any;
	flag : any;
  constructor(public viewCtrl : ViewController, public navCtrl: NavController, public navParams: NavParams, public alertCtrl : AlertController) {
  	this.ListOfInvoices = [];
  	this.peopleInfo = navParams.get("item");
  	this.index = navParams.get("index");
  	this.peopleInfo.Approved = false;
  	this.peopleInfo.Rejected = false;
  	this.flag = false;
    console.log(this.peopleInfo);
  }

  UpdateList(approved, rejected){
  	
  	


  		let listJson = localStorage.getItem('ListOfInvocies');
	  	if (listJson != null) {
	  		this.ListOfInvoices = JSON.parse(listJson);
	  	}
	  	
	  	if (this.ListOfInvoices) {
	  		this.ListOfInvoices.ListOfInvoices.ListOfInvoice.splice(this.index,1);
	  	}
	  	localStorage.setItem("ListOfInvocies", JSON.stringify(this.ListOfInvoices));
	  	//alert("Your invoice is approved");

	  	
	  	this.viewCtrl.dismiss('alert').then(() => {
	  		if (approved == true) {
		  		this.peopleInfo.Approved = approved;
				alert("Your invoice is approved");	  		
		  	}else if(rejected == true){
		  		this.peopleInfo.Rejected = rejected;
		  		alert("Your invoice is rejected");
		  	}

  			this.navCtrl.setRoot(ListPage,{loginStatus: false});
	  	})
  	
  }


  showConfirm(approved, rejected) {
    let status : string;
    if (approved == true) {
    	status = 'Approve';
    }else{
    	status = 'Reject';
    }
    this.confirm = this.alertCtrl.create({
      title: 'Are you sure to '+status+'?',
      message: '',//'Do you agree to use this lightsaber to do good across the intergalactic galaxy?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
      		this.flag = false;
          }
        },
        {
          text: 'Ok',
          handler: () => {
           this.UpdateList(approved, rejected);
            
          }
        }
      ]
    });
    this.confirm.present();
  }
}
