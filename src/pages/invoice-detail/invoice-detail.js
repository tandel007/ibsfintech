var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { ListPage } from '../../pages/list/list';
import { AlertController } from 'ionic-angular';
/**
 * Generated class for the InvoiceDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InvoiceDetailPage = /** @class */ (function () {
    function InvoiceDetailPage(viewCtrl, navCtrl, navParams, alertCtrl) {
        this.viewCtrl = viewCtrl;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.ListOfInvoices = [];
        this.peopleInfo = navParams.get("item");
        this.index = navParams.get("index");
        this.peopleInfo.Approved = false;
        this.peopleInfo.Rejected = false;
        this.flag = false;
        console.log(this.peopleInfo);
    }
    InvoiceDetailPage.prototype.UpdateList = function (approved, rejected) {
        var _this = this;
        var listJson = localStorage.getItem('ListOfInvocies');
        if (listJson != null) {
            this.ListOfInvoices = JSON.parse(listJson);
        }
        if (this.ListOfInvoices) {
            this.ListOfInvoices.ListOfInvoices.ListOfInvoice.splice(this.index, 1);
        }
        localStorage.setItem("ListOfInvocies", JSON.stringify(this.ListOfInvoices));
        //alert("Your invoice is approved");
        this.viewCtrl.dismiss('alert').then(function () {
            if (approved == true) {
                _this.peopleInfo.Approved = approved;
                alert("Your invoice is approved");
            }
            else if (rejected == true) {
                _this.peopleInfo.Rejected = rejected;
                alert("Your invoice is rejected");
            }
            _this.navCtrl.setRoot(ListPage, { loginStatus: false });
        });
    };
    InvoiceDetailPage.prototype.showConfirm = function (approved, rejected) {
        var _this = this;
        var status;
        if (approved == true) {
            status = 'Approve';
        }
        else {
            status = 'Reject';
        }
        this.confirm = this.alertCtrl.create({
            title: 'Are you sure to ' + status + '?',
            message: '',
            buttons: [
                {
                    text: 'Cancel',
                    handler: function () {
                        _this.flag = false;
                    }
                },
                {
                    text: 'Ok',
                    handler: function () {
                        _this.UpdateList(approved, rejected);
                    }
                }
            ]
        });
        this.confirm.present();
    };
    InvoiceDetailPage = __decorate([
        IonicPage(),
        Component({
            selector: 'page-invoice-detail',
            templateUrl: 'invoice-detail.html',
        }),
        __metadata("design:paramtypes", [ViewController, NavController, NavParams, AlertController])
    ], InvoiceDetailPage);
    return InvoiceDetailPage;
}());
export { InvoiceDetailPage };
//# sourceMappingURL=invoice-detail.js.map